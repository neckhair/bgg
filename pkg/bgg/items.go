package bgg

import "encoding/xml"

type Items struct {
	XMLName xml.Name     `xml:"items"`
	Items   []*Boardgame `xml:"item"`
	Total   int          `xml:"total,attr"`
}
