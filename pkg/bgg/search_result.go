package bgg

type SearchResult struct {
	Total      int
	Boardgames []*Boardgame
}
