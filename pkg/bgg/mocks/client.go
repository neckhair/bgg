package mocks

import (
	"net/url"

	"github.com/stretchr/testify/mock"
	"gitlab.com/neckhair/bgg/pkg/bgg"
)

type MockClient struct {
	mock.Mock
}

func (c *MockClient) Get(id string, params url.Values, v interface{}) error {
	args := c.Called(id, params, v)
	return args.Error(0)
}

func (c *MockClient) Search(query string, exact bool) (*bgg.SearchResult, error) {
	args := c.Called(query, exact)
	return args.Get(0).(*bgg.SearchResult), args.Error(1)
}

func (c *MockClient) Info(id string) (*bgg.Boardgame, error) {
	args := c.Called(id)
	return args.Get(0).(*bgg.Boardgame), args.Error(1)
}
