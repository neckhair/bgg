package bgg_test

import (
	"testing"

	"github.com/matryer/is"

	"gitlab.com/neckhair/bgg/pkg/bgg"
)

func TestBoardGameTitle(t *testing.T) {
	is := is.New(t)

	game := bgg.Boardgame{PrimaryName: "My Game", YearPublished: 2000}

	is.Equal(game.Title(), "My Game (2000)")
}

func TestBoardGameRankFormatted(t *testing.T) {
	is := is.New(t)

	game := bgg.Boardgame{Rank: 1}
	is.Equal(game.RankFormatted(), "1")

	game.Rank = 0
	is.Equal(game.RankFormatted(), "n/a")
}

func TestBoardGamePlaytimeFormatted(t *testing.T) {
	is := is.New(t)

	game := bgg.Boardgame{MaxPlayTime: 10, MinPlayTime: 10, PlayingTime: 10}
	is.Equal(game.PlaytimeFormatted(), "10min")

	game.MinPlayTime = 60
	game.MaxPlayTime = 90
	is.Equal(game.PlaytimeFormatted(), "60-90min")
}
