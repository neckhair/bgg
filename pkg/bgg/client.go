package bgg

import (
	"encoding/xml"
	"io"
	"net/http"
	"net/url"
)

const (
	BaseURL        = "https://api.geekdo.com/xmlapi2"
	SearchEndpoint = "/search"
	ThingEndpoint  = "/thing"

	DefaultType = "boardgame"
)

type Client struct{}

func (c *Client) Get(path string, params url.Values, v interface{}) error {
	u, _ := url.ParseRequestURI(BaseURL)
	u.Path += path
	u.RawQuery = params.Encode()
	urlStr := u.String()

	response, err := http.Get(urlStr)
	if err != nil {
		return err
	}

	body, err := io.ReadAll(response.Body)
	if err != nil {
		return err
	}

	return xml.Unmarshal(body, v)
}

func (c *Client) Search(query string, exact bool) (*SearchResult, error) {
	result := &SearchResult{}

	params := url.Values{}
	params.Add("type", DefaultType)
	params.Add("query", query)
	if exact {
		params.Add("exact", "1")
	}

	items := &Items{}
	err := c.Get(SearchEndpoint, params, items)
	if err != nil {
		return result, err
	}

	result.Total = items.Total
	result.Boardgames = items.Items

	return result, nil
}

func (c *Client) Info(id string) (*Boardgame, error) {
	params := url.Values{}
	params.Add("type", DefaultType)
	params.Add("id", id)
	params.Add("stats", "1")

	items := &Items{}
	err := c.Get(ThingEndpoint, params, items)
	if err != nil {
		return &Boardgame{}, err
	}

	if len(items.Items) < 1 {
		return nil, nil
	}
	return items.Items[0], nil
}
