package bgg

import (
	"encoding/xml"
	"fmt"
	"strconv"
)

const (
	LinkTypeBoardGameDesigner = "boardgamedesigner"
	LinkTypeBoardGameArtist   = "boardgameartist"
	LinkTypeBoardGameCategory = "boardgamecategory"
	BoardgameRankID           = 1
)

type Boardgame struct {
	ID            int
	Names         []string
	PrimaryName   string
	Description   string
	YearPublished int
	PlayingTime   int
	MinPlayTime   int
	MaxPlayTime   int
	MinAge        int
	Designers     []string
	Artists       []string
	Categories    []string
	Rank          int
	AverageRating float32
	AverageWeight float32
}

func (b *Boardgame) UnmarshalXML(d *xml.Decoder, start xml.StartElement) error {
	type intValueElement struct {
		Value int `xml:"value,attr"`
	}
	type floatValueElement struct {
		Value float32 `xml:"value,attr"`
	}
	var payload struct {
		ID    int `xml:"id,attr"`
		Names []struct {
			XMLName   xml.Name `xml:"name"`
			Type      string   `xml:"type,attr"`
			SortIndex int      `xml:"sortindex,attr"`
			Value     string   `xml:"value,attr"`
		} `xml:"name"`
		Description   string          `xml:"description"`
		YearPublished intValueElement `xml:"yearpublished"`
		PlayingTime   intValueElement `xml:"playingtime"`
		MinPlayTime   intValueElement `xml:"minplaytime"`
		MaxPlayTime   intValueElement `xml:"maxplaytime"`
		MinAge        intValueElement `xml:"minage"`
		Links         []struct {
			ID    int    `xml:"id,attr"`
			Value string `xml:"value,attr"`
			Type  string `xml:"type,attr"`
		} `xml:"link"`
		Statistics struct {
			Ratings struct {
				Average       floatValueElement `xml:"average"`
				AverageWeight floatValueElement `xml:"averageweight"`
				Ranks         struct {
					Rank []struct {
						ID           int    `xml:"id,attr"`
						Type         string `xml:"type,attr"`
						Value        string `xml:"value,attr"`
						Name         string `xml:"name,attr"`
						FriendlyName string `xml:"friendlyname,attr"`
					} `xml:"rank"`
				} `xml:"ranks"`
			} `xml:"ratings"`
		} `xml:"statistics"`
	}

	err := d.DecodeElement(&payload, &start)
	if err != nil {
		return err
	}

	b.ID = payload.ID
	b.YearPublished = payload.YearPublished.Value
	b.Description = payload.Description
	b.PlayingTime = payload.PlayingTime.Value
	b.MinPlayTime = payload.MinPlayTime.Value
	b.MaxPlayTime = payload.MaxPlayTime.Value
	b.MinAge = payload.MinAge.Value
	b.AverageRating = payload.Statistics.Ratings.Average.Value
	b.AverageWeight = payload.Statistics.Ratings.AverageWeight.Value

	for _, name := range payload.Names {
		b.Names = append(b.Names, name.Value)
		if name.Type == "primary" || b.PrimaryName == "" {
			b.PrimaryName = name.Value
		}
	}

	for _, rank := range payload.Statistics.Ratings.Ranks.Rank {
		if rank.ID == BoardgameRankID {
			b.Rank, _ = strconv.Atoi(rank.Value)
			break
		}
	}

	for _, link := range payload.Links {
		switch link.Type {
		case LinkTypeBoardGameCategory:
			b.Categories = append(b.Categories, link.Value)
		case LinkTypeBoardGameArtist:
			b.Artists = append(b.Artists, link.Value)
		case LinkTypeBoardGameDesigner:
			b.Designers = append(b.Designers, link.Value)
		}
	}

	return nil
}

/**
 * Returns a nicely formatted title of the game.
 **/
func (b *Boardgame) Title() string {
	return fmt.Sprintf("%s (%4d)", b.PrimaryName, b.YearPublished)
}

func (b *Boardgame) PlaytimeFormatted() string {
	if b.MinPlayTime != b.MaxPlayTime {
		return fmt.Sprintf("%d-%dmin", b.MinPlayTime, b.MaxPlayTime)
	} else {
		return fmt.Sprintf("%dmin", b.PlayingTime)
	}
}

func (b *Boardgame) RankFormatted() string {
	if b.Rank > 0 {
		return fmt.Sprintf("%d", b.Rank)
	} else {
		return "n/a"
	}
}
