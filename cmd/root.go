package cmd

import (
	"os"

	"github.com/spf13/cobra"
)

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "bgg",
	Short: "Access BoardGameGeek from the CLI.",
	Long: `bgg is an application that allows you to access boardgame information from your command
line.

$ bgg search brass
224517 - Brass: Bermingham

$ bgg info 224517
Brass: Birmingham (2018)

Designer:  Gavan Brown, Matt Tolman, Martin Wallace
Artist:    Lina Cossette, David Forest, Damien Mammoliti
Publisher: Roxley + 16 more

2-4 Players - 60-120 Minutes - Weight 3.90/5

By using this application you agree with the terms and rules of the BGG XMLAPI.`,
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}

func init() {
	// Here you will define your flags and configuration settings.
	// Cobra supports persistent flags, which, if defined here,
	// will be global for your application.

	// rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.bgg.yaml)")

	// Cobra also supports local flags, which will only run
	// when this action is called directly.
	rootCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
