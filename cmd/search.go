package cmd

import (
	"fmt"
	"net/url"

	"github.com/spf13/cobra"
	"gitlab.com/neckhair/bgg/pkg/bgg"
)

type BGGClient interface {
	Get(string, url.Values, interface{}) error
	Search(string, bool) (*bgg.SearchResult, error)
	Info(string) (*bgg.Boardgame, error)
}

var client BGGClient = &bgg.Client{}

// searchCmd represents the search command
var searchCmd = &cobra.Command{
	Use:   "search <query>",
	Args:  cobra.MinimumNArgs(1),
	Short: "Search for boardgames",
	Long: `Search for boardgames.

$ bgg game search brass
224517 - Brass: Bermingham`,
	RunE: func(cmd *cobra.Command, args []string) error {
		exactSearch, err := cmd.Flags().GetBool("exact")
		if err != nil {
			return err
		}

		result, err := client.Search(args[0], exactSearch)
		if err != nil {
			return err
		}

		for _, game := range result.Boardgames {
			fmt.Fprintf(cmd.OutOrStdout(), "%6d - %s\n", game.ID, game.Title())
		}
		fmt.Fprintf(cmd.OutOrStdout(), "Found %d results, showing %d.\n", result.Total, len(result.Boardgames))
		return nil
	},
}

func init() {
	gameCmd.AddCommand(searchCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// searchCmd.PersistentFlags().String("foo", "", "A help for foo")
	searchCmd.Flags().BoolP("exact", "x", false, "Exact search")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// searchCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
