package cmd

import (
	"bytes"
	"io"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/neckhair/bgg/pkg/bgg"
	"gitlab.com/neckhair/bgg/pkg/bgg/mocks"
)

func TestInfo(t *testing.T) {
	boardgameID := "167791"
	boardgame := &bgg.Boardgame{
		PrimaryName:   "Brass",
		YearPublished: 2018,
	}

	m := &mocks.MockClient{}
	m.On("Info", boardgameID).Return(boardgame, nil)

	client = m

	b := bytes.NewBufferString("")
	infoCmd.SetOut(b)
	err := infoCmd.RunE(infoCmd, []string{boardgameID})

	assert.Nil(t, err)
	m.AssertCalled(t, "Info", boardgameID)

	output, err := io.ReadAll(b)
	assert.Nil(t, err)
	assert.Contains(t, string(output), "Brass (2018)")
}
