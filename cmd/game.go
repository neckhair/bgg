package cmd

import (
	"github.com/spf13/cobra"
)

// gameCmd represents the game command
var gameCmd = &cobra.Command{
	Use:   "game",
	Short: "Search and show info for games",
}

func init() {
	rootCmd.AddCommand(gameCmd)
}
