package cmd

import (
	"fmt"
	"io"
	"strings"

	"github.com/spf13/cobra"
	"gitlab.com/neckhair/bgg/pkg/bgg"
)

// infoCmd represents the info command
var infoCmd = &cobra.Command{
	Use:   "info <id>",
	Args:  cobra.MinimumNArgs(1),
	Short: "Show information about a boardgame",
	Long: `Query the BGG API for information about a boardgame. You need to pass it an ID, which you
can retrieve with the "search" command. For example:

$ bgg game info 167791`,
	RunE: func(cmd *cobra.Command, args []string) error {
		id := args[0]

		boardgame, err := client.Info(id)
		if err != nil {
			return err
		}

		if boardgame == nil {
			fmt.Fprintln(cmd.OutOrStderr(), "No boardgame found with that id.")
			return nil
		}

		render(cmd.OutOrStdout(), boardgame)
		return nil
	},
}

func init() {
	gameCmd.AddCommand(infoCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// infoCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// infoCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}

func render(w io.Writer, game *bgg.Boardgame) {
	fmt.Fprintln(w, game.Title())

	fmt.Fprintln(w)

	fmt.Fprintf(w, "Designer: %s\n", strings.Join(game.Designers, ", "))
	fmt.Fprintf(w, "Artist:   %s\n", strings.Join(game.Artists, ", "))

	fmt.Fprintln(w)

	fmt.Fprintf(
		w,
		"Rank: %s - Rating: %0.2f - Weight: %0.2f - Playtime: %s\n",
		game.RankFormatted(),
		game.AverageRating,
		game.AverageWeight,
		game.PlaytimeFormatted(),
	)
}
