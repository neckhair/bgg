package cmd

import (
	"bytes"
	"io"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/neckhair/bgg/pkg/bgg"
	"gitlab.com/neckhair/bgg/pkg/bgg/mocks"
)

func TestSearch(t *testing.T) {
	boardgame := &bgg.Boardgame{
		YearPublished: 2016,
		PrimaryName:   "Brass",
	}

	m := &mocks.MockClient{}
	m.On("Search", "brass", false).Return(
		&bgg.SearchResult{Total: 1, Boardgames: []*bgg.Boardgame{boardgame}},
		nil,
	)

	client = m

	b := bytes.NewBufferString("")
	searchCmd.SetOut(b)

	err := searchCmd.RunE(searchCmd, []string{"brass"})
	assert.Nil(t, err)

	m.AssertCalled(t, "Search", "brass", false)

	output, err := io.ReadAll(b)
	assert.Nil(t, err)
	assert.Contains(t, string(output), "Brass")
}
