# BGG CLI

## Search

Search for a game and get a list of results.

```sh
  $ bgg game search "Brass"
  12345 - Brass: Birmingham (2018)
  34566 - Brass: Lancashire (2007)

  $ bgg game search --exact "Brass: Birmingham"
  12345 - Brass: Birmingham (2018)
```

## Info

Shows some info about a boardgame.

```sh
  $ bgg game info 12345
  Brass: Birmingham (2018)

  Designer:  Gavan Brown, Matt Tolman, Martin Wallace
  Artist:    Lina Cossette, David Forest, Damien Mammoliti
  Publisher: Roxley + 16 more

  Ranked 1. - 2-4 Players - 60-120 Minutes - Weight 3.90/5
```

## Plays

Shows the most recent plays of a boardgame.

```sh
  $ bgg plays --username neckhair 12345
  Brass: Birmingham (2018)

  * 01.06.2023
    Playtime: 120min
    User:     neckhair
```
